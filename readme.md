[![pipeline status](https://gitlab.com/rstumpner/hugo-ansible-azstorage-stumplo/badges/master/pipeline.svg)](https://gitlab.com/rstumpner/stumplo-hugo-pages/-/commits/master)

# Repository for hugo-ansible-azstorage-stumplo

This is the Repository of a example or hello world app. The main function ist to convert markdown files into a Website and Publish this content.The goal of this very simple web application is to show the architecture and mechanics of different technologies and deployment technics with their workflow.

In this Repository the technology stack is:
- Sources: markdown (/files) 
- application: Static Site Genereator [hugo.io](https://hugo.io)
- deployment: gitlab ci/cd and ansible (deployment framework)
- environment: Azure Storage Account

## Deployment Workflow
- Create a Gitlab Issue (plan)
- Clone the Gitlab Repository `git clone https://gitlab.com/rstumpner/hugo-ansible-azstorage-stumplo.git` (code)
- Create a Gitlab Merge Request
- change local Git Repository to the new branch 
- Create Changes in the Content directory `content/post/`
- commit changes to the new branch
- push changes to gitlab
- now the included CI/CD Pipeline from gitlab `.gitlab-ci.yml` generate a static website from the content
- merge the branch to main
- now the included CI/CD Pipeline from gitlab `.gitlab-ci.yml` publish the static website to the Gitlab Pages Service


## Details of the Ansible Deployment Framework for this Application
The Ansible Deployment Framework is a Template to have a easy setup for an Application Deployment with Ansible (https://gitlab.com/rstumpner/ansible-deploy-template) with a working CI/CD Pipeline in Gitlab.


## Feature Integrated CI/CD Pipeline 
Stages with Ansible tags for execution included:
- validate
- build
- static_tests
- prepare
- deploy

#### Validate:
Validates the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --syntax-check -vvvv`

#### Build:
Builds the configuration files from the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --tags build --vault-password-file /tmp/vault-password`

#### Inventory:
all.yml

#### Environments:
Some words about the Environments that are tested


## Feature Artifacts

Artifacts are collected on all stages . On the different stages there may be different artifacts like config files on the build stage and some reports on the deployment stage.

Artifacts path:

| path | description | Ansible tag|
|--- | --- |--- | 
| build | some artifacts from the build process (configs/packages)| [build]|
| test | Files or Reports from the Testing stage | [build] |
| state | some artifacts from the deployment process (diffs/reports) | [deploy] |
| docs | some artifacts from the generated documentation | [build,docs]|

 
License:
    MIT / BSD

Author Information:
roland@stumpner.at
